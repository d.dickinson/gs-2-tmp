title: GS2 Developer Meetings

The slides from recent GS2 Developer Meetings are available at the links below:

* [2019](https://zenodo.org/communities/gs2-dev-meeting-2019)
* [2018](https://bitbucket.org/gyrokinetics/gs2/wiki/GS2_Developers_Meeting_2018)

Full details of these and older meeting may be found on the [GS2 Wiki](https://bitbucket.org/gyrokinetics/gs2/wiki/GS2_Development_Meetings).
