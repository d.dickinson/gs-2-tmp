indexing benchmark
==================

This benchmarks the indexing routines for `e_lo` by repeatedly calling 
`it_idx`, `ik_idx`, `il_idx`, `is_idx`, `ig_idx`, `isign_idx` and `idx`. The
number of calls is controlled by `nstep`, with default 5000.

