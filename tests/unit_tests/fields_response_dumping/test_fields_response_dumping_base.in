!=================================================================
!                        GS2 INPUT FILE
!=================================================================
!
!  GS2 is a gyrokinetic flux tube initial value turbulence code
!  which can be used for fusion or astrophysical plasmas.
!
!  Website:
!            https://gyrokinetics.gitlab.io/gs2/
!  Repo:
!            https://bitbucket.org/gyrokinetics/gs2/
!  Citation:
!            https://zenodo.org/record/2551066
!  doi:
!            10.5281/zenodo.2551066
!
!=================================================================

!==============================
!GENERAL PARAMETERS  
!==============================
&parameters
 beta = 0.01 ! Ratio of particle to magnetic pressure (reference Beta, not total beta):  beta=n_0 T_0 /( B^2 / (8 pi))
 zeff = 1.0 ! Effective ionic charge.
/

&kt_grids_knobs
 grid_option = "box" ! The general layout of the perpendicular grid.
/

!==============================
!  
!==============================
&kt_grids_box_parameters
 y0 = -0.1
 ny = 4
 jtwist = 6
 nx = 6
/

!==============================
!  
!==============================
&theta_grid_parameters
 ntheta = 4 ! Number of points along field line (theta) per 2 pi segment
 nperiod = 1 ! Number of 2 pi segments along equilibrium magnetic field.
 eps = 0.18 ! eps=r/R
 epsl = 2.0 ! epsl=2 a/R
 shat = 0.8 ! 
 pk = 1.44 ! pk = 2 a / q R
 shift = 0.0 ! shift = -R q**2 dbeta/drho (>0)
/

!==============================
!  
!==============================
&theta_grid_knobs
 equilibrium_option = "s-alpha" ! Controls which geometric assumptions are used in the run.
/

!==============================
!PITCH ANGLE/ENERGY GRID SETUP  
!==============================
&le_grids_knobs
 ngauss = 3 ! Number of untrapped pitch-angles moving in one direction along field line.
 negrid = 12 ! Total number of energy grid points
/

!==============================
!BOUNDARY CONDITIONS  
!==============================
&dist_fn_knobs
 esv = .true.
 boundary_option = "linked" ! Sets the boundary condition along the field line (i.e. the boundary conditions at theta = +- pi).
 adiabatic_option = "iphi00=2" ! The form of the adiabatic response (if a species is being modeled as adiabatic).
/

!==============================
!  
!==============================
&knobs
 fphi = 1.0 ! Multiplies Phi (electrostatic potential).
 fapar = 1.0 ! Multiplies A_par. Use 1 for finite beta (electromagnetic), 0 otherwise (electrostatic)
 fbpar = 1.0
 delt = 0.05 ! Time step
 nstep = 20 ! Maximum number of timesteps
/

!==============================
!  
!==============================
&layouts_knobs
 layout = "xyles" ! 'yxles', 'lxyes', 'lyxes', 'lexys' Determines the way the grids are laid out in memory.
/

!==============================
!COLLISIONS  
!==============================
&collisions_knobs
 collision_model = "none" ! Collision model used in the simulation. Options: 'default', 'none', 'lorentz', 'ediffuse'
/

!==============================
!EVOLVED SPECIES  
!==============================
&species_knobs
 nspec = 1 ! Number of kinetic species evolved.
/

!==============================
!SPECIES PARAMETERS 1 
!==============================
&species_parameters_1
 z = 1.0 ! Charge
 mass = 1.0 ! Mass
 dens = 1.0 ! Density	
 temp = 1.0 ! Temperature
 tprim = 6.9 ! -1/T (dT/drho)
 fprim = 2.2 ! -1/n (dn/drho)
 uprim = 0.0 ! ?
 vnewk = 0.01 ! collisionality parameter
 type = "ion" ! Type of species, e.g. 'ion', 'electron', 'beam'
/

!==============================
! 1 
!==============================
&dist_fn_species_knobs_1
 fexpr = 0.45 ! Temporal implicitness parameter. Recommended value: 0.48
 bakdif = 0.05 ! Spatial implicitness parameter. Recommended value: 0.05
/

!==============================
!INITIAL CONDITIONS  
!==============================
&init_g_knobs
 clean_init = .true.
 chop_side = .false. ! Rarely needed. Forces asymmetry into initial condition.
 phiinit = 0.001 ! Average amplitude of initial perturbation of each Fourier mode.
 ginit_option = "default" ! Sets the way that the distribution function is initialized.
/

!==============================
!DIAGNOSTICS  
!==============================
&gs2_diagnostics_knobs
 print_flux_line = .F. ! Instantaneous fluxes output to screen
 write_nl_flux = .false. ! Write nonlinear fluxes as a function of time.
 print_line = .false. ! Estimated frequencies and growth rates to the screen/stdout
 write_verr = .false. ! Write velocity space diagnostics to '.lpc' and '.verr' files
 write_line = .false. ! If (write_ascii = T) write estimated frequencies and growth rates to the output file
 write_hrate = .false. ! Write heating rate, collisonal entropy generation etc to '.heat'
 write_avg_moments = .F. ! Write flux surface averaged low-order moments of g to runname.out.nc and runname.moments (if write_ascii = T)
 write_omega = .true. ! If (write_ascii = T) instantaneous omega to output file. Very heavy output
 write_omavg = .true. ! If (write_ascii = T) time-averaged growth rate and frequency to the output file.
 write_final_fields = .true. ! If (write_ascii = T) Phi(theta) written to '.fields'
 nsave = 100 ! Write restart files every nsave timesteps
 nwrite = 1 ! Output diagnostic data every nwrite
 navg = 10 ! Any time averages performed over navg
 omegatol = -0.001 ! The convergence has to be better than one part in 1/omegatol
 omegatinst = 500.0 ! Recommended value: 500.
 save_for_restart = .false. ! Write restart files.
 write_ascii = .false.
/

