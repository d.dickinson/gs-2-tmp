!> Main program. Used when running GS2 standalone, as opposed as a library for, e.g., Trinity.
!! Essentially this initializes a gs2_program_state_type 
!! object, and then calls the standard sequence of subroutines
!! from gs2_main to run the program. See gs2_main for more 
!! information.
!!
!! @note Currently FORD seems to ignore everything beyond the first line
!!
!! @note The following should probably be moved to a standalone file
!!
!! GS2 is an initial value nonlinear code which solves the gyrokinetic equation. This is the source code documentation for GS2, and is aimed at developers. 
!!
!! User Documentation
!! For a general introduction and user documentation please go to the Gyrokinetics Wiki: http://gyrokinetics.sourceforge.net/wiki/index.php/Main_Page
!! 
!! Some useful pages are: 
!!  - A beginners guide to downloading and installing: http://gyrokinetics.sourceforge.net/wiki/index.php/GS2:_A_Guide_for_Beginners
!!  - A general introduction to GS2: http://gyrokinetics.sourceforge.net/wiki/index.php/An_Introduction_to_GS2
!!  - A comprehensive guide to the GS2 input parameters: http://gyrokinetics.sourceforge.net/wiki/index.php/GS2_Input_Parameters
!!
!! @section doc Documentation Structure
!! Documentation is categorized by namespaces (i.e. modules), class (which in
!! fortran means custom types), and files. Within each namespace the subroutines
!! are documented in various ways, including developer comments and (very useful)
!! a chart showing which other subroutines  call the subroutine and which are
!! called by it.
!!
!! @section starting Starting Out
!!
!! If you want to start at the beginning and work out what GS2 does, start at
!! gs2.f90, and use the call graphs to follow the algorithm.
!!
!! @section Updating this documentation.
!! @section Updating Source Code Comments
!! This documentation is generated from commments added to the source code; to
!! add to it, add more comments to the <i>trunk</i> source. All documenting
!! comments begin with <tt>!></tt> and are continued with <tt>!!</tt>. For more help see the
!! doxygen help: http://www.stack.nl/~dimitri/doxygen/manual/docblocks.html
!!
!! : gen Updating this Documentation
!!
!! - Install doxygen: http://www.stack.nl/~dimitri/doxygen/
!! - Go to the trunk folder and type 
!! 
!!  <tt> make doc sync_doc USER=[sourceforge user name]</tt>
program gs2

  ! make_lib is a compiler flag used if running with 
  ! an old version of trinity (coupled flux tube code)
  ! MAKE_LIB is now deprecated.
# ifndef MAKE_LIB 
  !use optimisation_config, only: optimisation_type
  use gs2_optimisation, only: initialize_gs2_optimisation
  use gs2_optimisation, only: finalize_gs2_optimisation
  use gs2_optimisation, only: optimise_gs2
  use gs2_main, only: gs2_program_state_type
  use gs2_main, only: initialize_wall_clock_timer
  use gs2_main, only: initialize_gs2
  use gs2_main, only: initialize_equations
  use gs2_main, only: initialize_diagnostics
  use gs2_main, only: evolve_equations
  use gs2_main, only: run_eigensolver
  use gs2_main, only: finalize_diagnostics
  use gs2_main, only: finalize_equations
  use gs2_main, only: finalize_gs2
  use gs2_main, only: parse_command_line, write_used_inputs_file
  use exit_codes, only: EXIT_NSTEP, EXIT_NOT_REQUESTED
  use mp, only: proc0
  use file_utils, only: get_unused_unit, run_name
  use standard_header, only: standard_header_type
  implicit none
  type(gs2_program_state_type) :: state
  type(standard_header_type) :: local_header

  call parse_command_line()

  call initialize_gs2_optimisation(state)
  call initialize_wall_clock_timer

  if (state%optim%on) then
    call optimise_gs2(state)
  end if
  if (state%optim%auto .or. .not. state%optim%on) then
    local_header = standard_header_type()
    call initialize_gs2(state, header=local_header)
    call initialize_equations(state)
    call initialize_diagnostics(state, header=local_header)

    call write_used_inputs_file(header=local_header)

    state%print_times = .false.
    if (state%do_eigsolve) then 
      call run_eigensolver(state)
    else
       call evolve_equations(state, state%nstep)
       if (state%exit_reason%is_identical(EXIT_NOT_REQUESTED)) state%exit_reason = EXIT_NSTEP
    end if
    if(proc0) call state%exit_reason%write_exit_file()
    call finalize_diagnostics(state)
    call finalize_equations(state)
    state%print_times = .true.
    state%print_full_timers = .true.
    call finalize_gs2(state)
  end if

  call finalize_gs2_optimisation(state)

# else

  implicit none
  call run_gs2

# endif
end program gs2
